import {AuthService} from '../../src/auth/auth.service';
import {AuthController} from '../../src/auth/auth.controller';



describe('AuthController', () => {
    let AuthService: AuthService;
    let AuthController: AuthController;


    beforeEach(() => {
        AuthService = new AuthService();
        AuthController = new AuthController(AuthService);
    });

    describe('findAll', () => {
        it('return an array of fields', async () => {
            const result = ['test'];
            jest.spyOn(AuthService, 'findAll').mockImplementation(() => result);

            expect(await AuthController.findAll()).toBe(result);
        });
    });
});
