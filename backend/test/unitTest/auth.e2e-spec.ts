import * as request from 'supertest';
import {Test} from '@nestjs/testing';
import {AuthModule} from '../../src/auth/auth.module';
import {AuthService} from '../../src/auth/auth.service';
import { INestApplication } from '@nestjs/common';

describe('Auth', () => {
    let app: INestApplication;
    let AppService = { findAll: () => ['test'] };

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            imports: [AuthModule],
        })
        .overrideProvider(AuthService)
        .useValue(AuthService)
        .compile();

    app = module.createNestApplication();
    await app.init();
    });

    it('/GET auth', () => {
        return request(app.getHttpServer())
        .get('/register')
        .expect(3030)
        .expect({
            data: AuthService.findAll(),
        });
    });

    afterAll(async () => {
        await app.close();
    })
} )
