import {AppController} from '../../src/app.controller';
import {AppService} from '../../src/app.service';


describe('AppController', () => {
    let AppController: AppController;
    let AppService: AppService;

    beforeEach(() => {
        AppService = new AppService();
        AppController = new AppController(AppService); 
    });

    describe('findAll', () => {
        it('return array of app serv', async () => {
            const result = ['test'];
            jest.spyOn(AppService, 'findAll').mockImplementation(() => result);

            expect(await AppController.findAll()).toBe(result);
        });
    });
});